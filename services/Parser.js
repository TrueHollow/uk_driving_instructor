const puppeteer = require('puppeteer-extra');
const pluginStealth = require('puppeteer-extra-plugin-stealth');
const logger = require('../logger')('services/Parser.js');

puppeteer.use(pluginStealth());

const URL =
  'https://finddrivinginstructor.dvsa.gov.uk/DSAFindNearestWebApp/findNearest.form?lang=en';
const NUMBER_OF_ATTEMPTS = 5;
const typeInputDelay = 50;
const VALID_TITLE = 'Driving instructor search: GOV.UK';

class Parser {
  /**
   * Constructor
   * @param {Object} configuration
   */
  constructor(configuration) {
    this.browserConfiguration = configuration;
    this.browser = null;
  }

  async clean() {
    if (this.browser) {
      logger.debug('Closing browser');
      await this.browser.close();
      this.browser = null;
    }
  }

  async getNewPage() {
    const page = await this.browser.newPage();
    await page.setRequestInterception(true);
    page.on('request', async request => {
      if (!request.isNavigationRequest()) {
        request.continue();
        return;
      }
      const headers = request.headers();
      headers['Accept-Language'] = 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7';
      request.continue({ headers });
    });
    return page;
  }

  async init() {
    await this.clean();
    logger.debug('Opening new browser');
    this.browser = await puppeteer.launch(this.browserConfiguration);
  }

  static async OpenSite(page, postcode) {
    let result = false;
    let validTitleCounter = 0;
    await page.goto(URL, { waitUntil: 'networkidle0', timeout: 0 });

    for (let i = 0; i < NUMBER_OF_ATTEMPTS; i += 1) {
      try {
        // eslint-disable-next-line no-await-in-loop
        await page.goto(URL, { waitUntil: 'networkidle0', timeout: 0 });
        const cookies = await page.cookies();
        logger.debug(cookies);

        // eslint-disable-next-line no-await-in-loop
        const title = await page.title();
        if (title === VALID_TITLE) {
          validTitleCounter += 1;
        }
        if (validTitleCounter > 2) {
          result = true;
          break;
        }
      } catch (e) {
        logger.error(`Error on opening site (${postcode}): ${e}`);
      }
    }
    return result;
  }

  static async ParseResult(page) {
    return page.evaluate(() => {
      const result = [];
      // eslint-disable-next-line no-undef
      const blocks = document.querySelectorAll('#js-live-search-results li');
      blocks.forEach(item => {
        let name = null;
        const nameEl = item.querySelector('.instructor-name');
        if (nameEl) {
          name = nameEl.textContent.trim();
        }

        let email = null;
        const emailEl = item.querySelector('.email');
        if (emailEl) {
          const attr = emailEl.getAttribute('href');
          if (attr) {
            email = attr.substr(7); // skipping `email:`
          }
        }

        let phoneNumber = null;
        const phoneEl = item.querySelector('.phone');
        if (phoneEl) {
          phoneNumber = phoneEl.textContent.trim();
        }
        result.push({
          name,
          email,
          phone_number: phoneNumber,
        });
      });
      return result;
    });
  }

  static async HasNextButton(page, postcode) {
    const itHas = await page.evaluate(() => {
      // eslint-disable-next-line no-undef
      return document.querySelector('.pagination .next');
    });
    if (itHas) {
      logger.debug(`Found next page link for ${postcode}. Clicking on it.`);
      await Promise.all([
        page.waitForNavigation(),
        page.click('.pagination .next', { delay: typeInputDelay }),
      ]);
      return true;
    }
    logger.debug(`No next page link for ${postcode}`);
    return false;
  }

  /**
   * Get data by post codes.
   * @param {string,null} postcode
   */
  async getDataPostCode(postcode) {
    const page = await this.getNewPage();
    logger.debug(`Opening link for post code ${postcode}`);
    const linkOpened = await Parser.OpenSite(page, postcode);
    if (linkOpened) {
      logger.info(`Site for ${postcode} was opened successfully.`);
    } else {
      await page.close();
      logger.info(`Site for ${postcode} wasn't opened.`);
      return null;
    }
    logger.debug(`Typing postcode: ${postcode}`);
    await page.click('#postcode', { delay: typeInputDelay });
    await page.keyboard.type(postcode, { delay: typeInputDelay });

    await Promise.all([
      page.waitForNavigation(),
      page.keyboard.press('Enter', { delay: typeInputDelay }),
    ]);

    let result = [];
    let nextPage = false;
    do {
      // eslint-disable-next-line no-await-in-loop
      result = result.concat(await Parser.ParseResult(page));
      // eslint-disable-next-line no-await-in-loop
      nextPage = await Parser.HasNextButton(page, postcode);
    } while (nextPage);

    logger.debug(
      `Parsing pages finished for ${postcode}. Result have: ${result.length}`
    );
    await page.close();
    return result;
  }
}

module.exports = Parser;
