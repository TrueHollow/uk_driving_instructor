const Parser = require('./services/Parser');
const config = require('./config');
const logger = require('./logger')('index.js');

logger.info('Script started');

const main = async () => {
  const parser = new Parser(config.chrome);
  await parser.init();

  let result = await parser.getDataPostCode('UB1 2LH');
  logger.info('Result:');
  logger.info(result);

  result = await parser.getDataPostCode('BR1 1AD');
  logger.info('Result:');
  logger.info(result);

  await parser.clean();
};

main().then(() => logger.info('Script finished'));
